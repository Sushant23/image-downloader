CREATE DATABASE IF NOT EXISTS myntra_simple;

use myntra_simple;

CREATE TABLE IF NOT EXISTS `myntra_order` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(50) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `last_modified_on` datetime DEFAULT NULL,
  `version` int(11) DEFAULT '0',
  `login_id` varchar(255) NOT NULL,
  `mrp` decimal(10,2) NOT NULL DEFAULT '0.00',
  `status` varchar(10) NOT NULL DEFAULT 'CREATED',
  `discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `cancellation_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `order_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(50) DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `last_modified_on` datetime NOT NULL,
  `version` bigint(20) DEFAULT NULL,
  `coupon_discount` double DEFAULT NULL,
  `mrp` double DEFAULT NULL,
  `order_id` bigint(20) DEFAULT NULL,
  `quantity` bigint(20) DEFAULT NULL,
  `sku_id` bigint(20) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `trade_discount` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `cancellation_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(50) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `last_modified_on` datetime DEFAULT NULL,
  `version` int(11) DEFAULT '0',
  `code` int(11) NOT NULL,
  `description` varchar(512) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  UNIQUE KEY `myUniqueConstraint` (`description`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

