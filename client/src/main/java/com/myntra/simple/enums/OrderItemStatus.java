package com.myntra.simple.enums;

/**
 * Created by pavankumar.at on 03/08/15.
 */
public enum OrderItemStatus {

    DELIVERED("Delivered"),
    SHIPPED("Shipped"),
    CREATED("Created"),
    CANCELLED("Cancelled");

    private String type;

    private OrderItemStatus(String type) {
        this.type = type;
    }

    public String getStatusDescription() {
        return type;
    }

}
