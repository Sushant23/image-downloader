package com.myntra.simple.enums;

/**
 * Created by pavankumar.at on 15/07/15.
 */
public enum OrderStatus {

    CONFIRMED("Confirmed"),
    DELIVERED("Delivered"),
    SHIPPED("Shipped"),
    CREATED("Created"),
    DECLINED("Declined"),
    CANCELLED("Cancelled");

    private String type;

    private OrderStatus(String type) {
        this.type = type;
    }

    public String getStatusDescription() {
        return type;
    }

}
