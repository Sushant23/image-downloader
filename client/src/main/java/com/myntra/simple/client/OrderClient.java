package com.myntra.simple.client;

import com.myntra.commons.auth.ContextInfo;
import com.myntra.commons.client.BaseWebClient;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.utils.Context;
import com.myntra.simple.client.response.OrderResponse;

/**
 * Created by pavankumar.at on 15/07/15.
 */
public class OrderClient {

    public static final String SERVICE_PREFIX = "/simple/order/";

    public static OrderResponse getOrder(String serviceUrl, Long orderId, ContextInfo contextInfo) throws ERPServiceException {
        BaseWebClient client = new BaseWebClient(serviceUrl, "simpleURL", contextInfo);
        client.path(SERVICE_PREFIX + orderId);
        return client.get(OrderResponse.class);
    }

    public static OrderResponse confirmOrder(String serviceUrl, Long orderId, ContextInfo contextInfo) throws ERPServiceException {
        BaseWebClient client = new BaseWebClient(serviceUrl, "simpleURL", contextInfo);
        client.path(SERVICE_PREFIX + "confirm/" + orderId);
        return client.put(null, OrderResponse.class);
    }

    public static OrderResponse shipOrder(String serviceUrl, Long orderId, ContextInfo contextInfo) throws ERPServiceException {
        BaseWebClient client = new BaseWebClient(serviceUrl, "simpleURL", contextInfo);
        client.path(SERVICE_PREFIX + "ship/" + orderId);
        return client.put(null, OrderResponse.class);
    }

    public static OrderResponse deliverOrder(String serviceUrl, Long orderId, ContextInfo contextInfo) throws ERPServiceException {
        BaseWebClient client = new BaseWebClient(serviceUrl, "simpleURL", contextInfo);
        client.path(SERVICE_PREFIX + "deliver/" + orderId);
        return client.put(null, OrderResponse.class);
    }

    public static OrderResponse declineOrder(String serviceUrl, Long orderId, ContextInfo contextInfo) throws ERPServiceException {
        BaseWebClient client = new BaseWebClient(serviceUrl, "simpleURL", contextInfo);
        client.path(SERVICE_PREFIX + "decline/" + orderId);
        return client.put(null, OrderResponse.class);
    }

    public static OrderResponse cancelOrder(String serviceUrl, Long orderId, ContextInfo contextInfo) throws ERPServiceException {
        BaseWebClient client = new BaseWebClient(serviceUrl, "simpleURL", contextInfo);
        client.path(SERVICE_PREFIX + "cancel/" + orderId);
        return client.put(null, OrderResponse.class);
    }

}