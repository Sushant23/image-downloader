package com.myntra.simple.client.entry;

import com.myntra.commons.entries.BaseEntry;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by vedulla.krishna - 11153 on 17/08/15.
 */
@XmlRootElement(name = "cancellationReason")
public class CancellationReasonEntry extends BaseEntry{

    private int code;

    private String description;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "OrderEntry{"+
                " code: " + code +
                ", description: " + description+
                "}";
    }
}
