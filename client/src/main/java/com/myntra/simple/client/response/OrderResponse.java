package com.myntra.simple.client.response;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.simple.client.entry.OrderEntry;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by pavankumar.at on 15/07/15.
 */
@XmlRootElement(name = "orderResponse")
public class OrderResponse extends AbstractResponse {

    private List<OrderEntry> order;

    @XmlElementWrapper(name = "data")
    @XmlElement(name = "order")
    public List<OrderEntry> getData() {
        return order;
    }

    public final void setData(List<OrderEntry> order) {
        this.order = order;
        if(getStatus()!=null && order != null){
            getStatus().setTotalCount(order.size());
        }
    }

    public OrderResponse() { super();}


    public OrderResponse(StatusResponse status) {
        super(status);
    }

    public OrderResponse(List<OrderEntry> orderEntry) {
        super();
        setData(orderEntry);
    }

    @Override
    public String toString() {
        return "OrderResponse{" +
                "order=" + order +
                '}';
    }
}
