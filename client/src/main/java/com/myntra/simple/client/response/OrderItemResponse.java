package com.myntra.simple.client.response;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.simple.client.entry.OrderItemEntry;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by pavankumar.at on 05/08/15.
 */
@XmlRootElement(name = "orderResponse")
public class OrderItemResponse extends AbstractResponse{

    private List<OrderItemEntry> orderItem;

    @XmlElementWrapper(name = "data")
    @XmlElement(name = "orderItem")
    public List<OrderItemEntry> getData() {
        return orderItem;
    }

    public final void setData(List<OrderItemEntry> orderItemEntries) {
        this.orderItem = orderItemEntries;
        if(getStatus()!=null && orderItemEntries != null){
            getStatus().setTotalCount(orderItemEntries.size());
        }
    }

    public OrderItemResponse() { super();}


    public OrderItemResponse(StatusResponse status) {
        super(status);
    }

    public OrderItemResponse(List<OrderItemEntry> orderItemEntry) {
        super();
        setData(orderItemEntry);
    }

    @Override
    public String toString() {
        return "OrderItemResponse{" +
                "orderItem=" + orderItem +
                '}';
    }

}
