package com.myntra.simple.client.codes;

import com.myntra.commons.codes.ERPErrorCodes;
import com.myntra.commons.codes.StatusCodes;

/**
 * Created by pavankumar.at on 15/07/15.
 */
public class SimpleErrorCodes extends ERPErrorCodes {

    private static final String BUNDLE_NAME = "SimpleErrorCodes";

    public SimpleErrorCodes(int code, String message) {
        setAll(code, message, BUNDLE_NAME);
    }

    public static final StatusCodes ORDER_NOT_FOUND_HERE = new SimpleErrorCodes(1000, "ORDER_NOT_FOUND_HERE");
    public static final StatusCodes INVALID_ORDER_STATUS= new SimpleErrorCodes(1001, "INVALID_ORDER_STATUS");
    public static final StatusCodes ORDER_NOT_CONFIRMED = new SimpleErrorCodes(1002, "ORDER_NOT_CONFIRMED");
    public static final StatusCodes ORDER_ALREADY_CANCELLED = new SimpleErrorCodes(1003, "ORDER_ALREADY_CANCELLED");
    public static final StatusCodes ORDER_NOT_SHIPPED = new SimpleErrorCodes(1004, "ORDER_NOT_SHIPPED");
    public static final StatusCodes ORDER_ALREADY_DELIVERED = new SimpleErrorCodes(1005, "ORDER_ALREADY_DELIVERED");
    public static final StatusCodes ORDER_ALREADY_SHIPPED = new SimpleErrorCodes(1006, "ORDER_ALREADY_SHIPPED");
    public static final StatusCodes ORDER_ALREADY_DECLINED = new SimpleErrorCodes(1006, "ORDER_ALREADY_DECLINED");

}
