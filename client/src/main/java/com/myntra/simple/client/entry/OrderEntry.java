package com.myntra.simple.client.entry;

import com.myntra.commons.entries.BaseEntry;
import com.myntra.simple.enums.OrderStatus;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by pavankumar.at on 15/07/15.
 */
@XmlRootElement(name = "order")
public class OrderEntry extends BaseEntry {

    private String loginId;

    private Double mrp;

    private OrderStatus status;

    private Double discount;

    private String statusDescription;

    private Double finalPrice;

    private String cancellationDescription;

    private Long cancellationId;

    private List<OrderItemEntry> orderItemEntries;

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public Double getMrp() {
        return mrp;
    }

    public void setMrp(Double mrp) {
        this.mrp = mrp;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public Double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(Double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getCancellationDescription() {
        return cancellationDescription;
    }

    public void setCancellationDescription(String cancellationDescription) {
        this.cancellationDescription = cancellationDescription;
    }

    public Long getCancellationId() {
        return cancellationId;
    }

    public void setCancellationId(Long cancellationId) {
        this.cancellationId = cancellationId;
    }

    public List<OrderItemEntry> getOrderItemEntries() {
        return orderItemEntries;
    }

    public void setOrderItemEntries(List<OrderItemEntry> orderItemEntries) {
        this.orderItemEntries = orderItemEntries;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderEntry that = (OrderEntry) o;

        if (discount != null ? !discount.equals(that.discount) : that.discount != null) return false;
        if (finalPrice != null ? !finalPrice.equals(that.finalPrice) : that.finalPrice != null) return false;
        if (!loginId.equals(that.loginId)) return false;
        if (mrp != null ? !mrp.equals(that.mrp) : that.mrp != null) return false;
        if (orderItemEntries != null ? !orderItemEntries.equals(that.orderItemEntries) : that.orderItemEntries != null)
            return false;
        if (status != that.status) return false;
        if (statusDescription != null ? !statusDescription.equals(that.statusDescription) : that.statusDescription != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = loginId.hashCode();
        result = 31 * result + (mrp != null ? mrp.hashCode() : 0);
        result = 31 * result + status.hashCode();
        result = 31 * result + (discount != null ? discount.hashCode() : 0);
        result = 31 * result + (statusDescription != null ? statusDescription.hashCode() : 0);
        result = 31 * result + (finalPrice != null ? finalPrice.hashCode() : 0);
        result = 31 * result + (orderItemEntries != null ? orderItemEntries.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "OrderEntry{" +
                "loginId='" + loginId + '\'' +
                ", mrp=" + mrp +
                ", status=" + status +
                ", discount=" + discount +
                ", statusDescription='" + statusDescription + '\'' +
                ", finalPrice=" + finalPrice +
                ", cancellationDescription=" + cancellationDescription +
                ", orderItemEntries=" + orderItemEntries +
                '}';
    }
}
