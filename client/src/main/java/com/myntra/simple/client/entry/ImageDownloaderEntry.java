package com.myntra.simple.client.entry;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by 11118 on 10/05/16.
 */
public class ImageDownloaderEntry {

    public String getSourceImageURL() {
        return sourceImageURL;
    }

    public void setSourceImageURL(String sourceImageURL) {
        this.sourceImageURL = sourceImageURL;
    }

    @JsonProperty("image_url")
    private String sourceImageURL;

    @Override
    public String toString() {
        return "ImageDownloaderEntry{" +
                "image_url='" + getSourceImageURL() + '\'' +
                '}';
    }

}
