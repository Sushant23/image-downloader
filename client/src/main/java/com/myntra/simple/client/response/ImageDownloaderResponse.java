package com.myntra.simple.client.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.myntra.commons.codes.StatusResponse;

/**
 * Created by 11118 on 10/05/16.
 */
public class ImageDownloaderResponse {

    public String getSourceImageURL() {
        return sourceImageURL;
    }

    public void setSourceImageURL(String sourceImageURL) {
        this.sourceImageURL = sourceImageURL;
    }

    public String getS3ImageURL() {
        return S3ImageURL;
    }

    public void setS3ImageURL(String s3ImageURL) {
        S3ImageURL = s3ImageURL;
    }

    public StatusResponse getResponse() {
        return response;
    }

    public void setResponse(StatusResponse response) {
        this.response = response;
    }

    @JsonProperty("source_image_url")
    private String sourceImageURL;

    @JsonProperty("s3_url")
    private String S3ImageURL;

    @JsonProperty("response")
    StatusResponse response;

    @Override
    public String toString() {
        return "ImageDownloaderResponse{" +
                "source_image_url='" + getSourceImageURL() + '\'' +
                ",\"s3_url\":" + getS3ImageURL()  +
                ",\"response_code\":" + getResponse().getStatusCode()  +
                ",\"response_type\":" + getResponse().getStatusType()  +
                ",\"response_message\":" + getResponse().getStatusMessage()  +
                "}";
    }

}
