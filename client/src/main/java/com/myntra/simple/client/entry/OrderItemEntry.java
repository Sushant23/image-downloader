package com.myntra.simple.client.entry;

import com.myntra.commons.entries.BaseEntry;
import com.myntra.simple.enums.OrderItemStatus;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by pavankumar.at on 03/08/15.
 */
@XmlRootElement(name = "orderItem")
public class OrderItemEntry  extends BaseEntry {

    private Double mrp;

    private Long skuId;

    private OrderItemStatus status;

    private Double tradeDiscount;

    private Double couponDiscount;

    private Long quantity;

    private Long orderId;

    public Double getMrp() {
        return mrp;
    }

    public void setMrp(Double mrp) {
        this.mrp = mrp;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public OrderItemStatus getStatus() {
        return status;
    }

    public void setStatus(OrderItemStatus status) {
        this.status = status;
    }

    public Double getTradeDiscount() {
        return tradeDiscount;
    }

    public void setTradeDiscount(Double tradeDiscount) {
        this.tradeDiscount = tradeDiscount;
    }

    public Double getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(Double couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "OrderItemEntry{" +
                "mrp=" + mrp +
                ", skuId=" + skuId +
                ", status=" + status +
                ", tradeDiscount=" + tradeDiscount +
                ", couponDiscount=" + couponDiscount +
                ", quantity=" + quantity +
                ", orderId=" + orderId +
                '}';
    }
}
