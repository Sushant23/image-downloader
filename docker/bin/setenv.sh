export CATALINA_PID="/myntra/pid/image-downloader-service.pid"
export JRE_HOME="/usr/java/java8"
export JAVA_OPTS="-Xms512m -Xmx2048m -XX:MaxPermSize=256m -Dlog4j.configurationFile=conf/log4j2.xml"
