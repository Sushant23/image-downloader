package com.myntra.simple.listeners;

import com.myntra.kafka.consumer.KafkaConsumerClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by 11118 on 01/02/16.
 */

@Component
public class SpringContextListener implements ApplicationListener<ContextRefreshedEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpringContextListener.class);

    public static JedisCluster jedisCluster = null;

    @Autowired
    KafkaConsumerClient kafkaConsumerClient;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextStartedEvent) {
        LOGGER.info("INSIDE LISTENER");
        Set<HostAndPort> jedisClusterNodes = new HashSet<HostAndPort>();

        // TODO: Load from conf folder if required
        LOGGER.info("Loading Redis Configs...");
        jedisClusterNodes.add(new HostAndPort("10.162.35.155", 7001));
        jedisClusterNodes.add(new HostAndPort("10.162.35.155", 7002));
        jedisClusterNodes.add(new HostAndPort("10.162.35.155", 7003));

        jedisCluster = new JedisCluster(jedisClusterNodes);

        LOGGER.info("******************starting consumer******************");

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                LOGGER.info("Entering thread");
                kafkaConsumerClient.pullFromQueue();
                LOGGER.info("leaving thread");
            }
        });
        t.start();

    }

}