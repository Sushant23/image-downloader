package com.myntra.simple.message.listener;

import com.myntra.kafka.listener.KafkaListener;
import com.myntra.simple.client.entry.ImageDownloaderEntry;
import com.myntra.simple.handler.FetchS3ResourceHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by 11118 on 11/05/16.
 */
public class FetchS3ResourceMessageListener implements KafkaListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(FetchS3ResourceMessageListener.class);


    private FetchS3ResourceHandler handler = FetchS3ResourceHandler.getInstance();

    private ExecutorService executorService;

    public FetchS3ResourceMessageListener(int numThreads, int queueSize) {
        executorService = new ThreadPoolExecutor(numThreads, numThreads, 100000L, TimeUnit.MILLISECONDS,
                new ArrayBlockingQueue<Runnable>(queueSize, true), new ThreadPoolExecutor.CallerRunsPolicy());
    }

    @Override
    public void processMessage(String s) {

        executorService.submit(() -> {
            try {
                LOGGER.info("Incoming message to Kafka is: " + s);
                ImageDownloaderEntry entry = new ImageDownloaderEntry();
                entry.setSourceImageURL(s);
                LOGGER.info("Sending message for processing.");
                handler.fetchS3Resource(entry);
            } catch (Exception ex) {
                LOGGER.info("Exception occurred while processing message:" + ex.getMessage());
                LOGGER.error("Exception: ", ex);
            }
        });
    }

    @Override
    public void processMessages(List<String> list) {

    }
}
