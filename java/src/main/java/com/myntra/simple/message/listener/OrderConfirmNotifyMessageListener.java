package com.myntra.simple.message.listener;

import com.myntra.commons.amqp.AbstractMessageListener;
import com.myntra.simple.client.entry.OrderEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by pavankumar.at on 19/08/15.
 */
public class OrderConfirmNotifyMessageListener extends AbstractMessageListener<OrderEntry> {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void doProcess(OrderEntry orderEntry) throws Exception {
        logger.info("Received order message: " + orderEntry);
        // Fetch data from here and there
        // Send message to notification service to send SMS / email
        logger.info("Sent email / sms for order: " + orderEntry.getId());
    }
}
