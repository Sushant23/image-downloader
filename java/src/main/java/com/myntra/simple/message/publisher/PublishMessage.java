package com.myntra.simple.message.publisher;

import com.myntra.commons.amqp.AfterCommitAMQPMessagePublisher;
import com.myntra.commons.utils.ApplicationContextHelper;
import com.myntra.simple.client.entry.OrderEntry;

/**
 * Created by Bhaskar Saraogi on 19/08/15.
 */
public class PublishMessage {

    private static final AfterCommitAMQPMessagePublisher afterCommitAMQPMessagePublisher = ApplicationContextHelper.getBean("afterCommitAmqpMessagePublisher", AfterCommitAMQPMessagePublisher.class);

    public static void sendOrderConfirmNotification(OrderEntry orderEntry) {
        afterCommitAMQPMessagePublisher.sendMessageToChannel(orderEntry,"notificationChannel");
    }
}
