package com.myntra.simple.manager.impl;

import com.myntra.commons.exception.ManagerException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.simple.client.entry.CancellationReasonEntry;
import com.myntra.simple.entities.CancellationReason;
import com.myntra.simple.manager.CancellationReasonManager;
import org.springframework.cache.annotation.Cacheable;

/**
 * Created by vedulla.krishna - 11153 on 17/08/15.
 */
public class CancellationReasonManagerImpl extends BaseManagerImpl<CancellationReasonEntry,
        CancellationReason> implements CancellationReasonManager {

        @Override
        @Cacheable(value = "cancellationReason")
        public CancellationReasonEntry findById(Long entityId) throws ManagerException {
                return super.findById(entityId);
        }
}
