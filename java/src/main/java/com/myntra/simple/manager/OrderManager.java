package com.myntra.simple.manager;

import com.myntra.commons.exception.ManagerException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.simple.client.entry.OrderEntry;
import com.myntra.simple.entities.Order;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by pavankumar.at on 15/07/15.
 */
public interface OrderManager extends BaseManager<OrderEntry, Order>{

    public List<OrderEntry> getRandom(Long count) throws ManagerException;

    public String getRegionCode(Long pincode) throws ManagerException;

    @Transactional(rollbackFor = Exception.class)
    public OrderEntry confirmOrder(Long orderId) throws ManagerException;

}
