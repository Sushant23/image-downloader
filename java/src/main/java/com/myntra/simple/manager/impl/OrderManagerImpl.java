package com.myntra.simple.manager.impl;

import com.myntra.client.notification.EmailServiceClient;
import com.myntra.client.notification.utils.MimeType;
import com.myntra.commons.auth.ContextInfo;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.exception.ManagerException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.utils.Context;
import com.myntra.commons.utils.PaginatedList;
import com.myntra.lms.client.PincodeDataSetupServiceClient;
import com.myntra.lms.client.response.PincodeResponse;
import com.myntra.simple.client.codes.SimpleErrorCodes;
import com.myntra.simple.client.entry.OrderEntry;
import com.myntra.simple.client.entry.OrderItemEntry;
import com.myntra.simple.entities.Order;
import com.myntra.simple.entities.OrderItem;
import com.myntra.simple.enums.OrderStatus;
import com.myntra.simple.manager.CancellationReasonManager;
import com.myntra.simple.manager.OrderItemManager;
import com.myntra.simple.manager.OrderManager;
import com.myntra.simple.message.publisher.PublishMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pavankumar.at on 15/07/15.
 */
public class OrderManagerImpl extends BaseManagerImpl<OrderEntry, Order> implements OrderManager {

    private OrderItemManager orderItemManager;

    private CancellationReasonManager cancellationReasonManager;

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderManagerImpl.class.getName());

    public CancellationReasonManager getcancellationReasonManager() {
        return cancellationReasonManager;
    }

    public void setcancellationReasonManager(CancellationReasonManager cancellationReasonManager) {
        this.cancellationReasonManager = cancellationReasonManager;
    }

    @Override
    public OrderEntry convertToEntry(Order order) {
        OrderEntry orderEntry = super.convertToEntry(order);
        orderEntry.setFinalPrice(order.getMrp() - order.getDiscount());
        orderEntry.setStatusDescription(order.getStatus().getStatusDescription());
        List<OrderItemEntry> orderItemEntryList = new ArrayList<>();
        for (OrderItem orderItem : order.getOrderItems()) {
            orderItemEntryList.add(orderItemManager.convertToEntry(orderItem));
        }

        try {
            orderEntry.setCancellationDescription(cancellationReasonManager.findById(orderEntry.getCancellationId()).getDescription());
        } catch (ManagerException e){
            System.out.println("Unable to retrieve from Cancellation Details");
           // LOGGER.error("Unable to retrieve from Cancellation Details", e);
        }

        orderEntry.setOrderItemEntries(orderItemEntryList);
        return orderEntry;
    }

    @Override
    public List<OrderEntry> getRandom(Long count) throws ManagerException {
        PaginatedList<OrderEntry> data = search(0, count.intValue(), "", "", "status.eq:CONFIRMED");
        if (data != null && data.getResults() != null && !data.getResults().isEmpty()) {
            return data.getResults();
        }
        return null;
    }

    @Override
    public String getRegionCode(Long pincode) throws ManagerException {
        try {
            PincodeResponse pincodeResponse = PincodeDataSetupServiceClient.findById(null, pincode, Context.getContextInfo());
            return pincodeResponse.getPincodes().get(0).getRegionCode();
        } catch (ERPServiceException e) {
            LOGGER.error("Unable to retrieve region code", e);
            throw new ManagerException(SimpleErrorCodes.GENERIC_ERROR);
        }
    }

    @Override
    public OrderEntry confirmOrder(Long orderId) throws ManagerException {
        OrderEntry orderEntry = findById(orderId);
        if (!OrderStatus.CONFIRMED.equals(orderEntry.getStatus())) {
            if (OrderStatus.DECLINED.equals(orderEntry.getStatus())) {
                throw new ManagerException(SimpleErrorCodes.ORDER_ALREADY_DECLINED, new String[]{orderId.toString(),OrderStatus.CONFIRMED.name()});
            } else if (OrderStatus.CANCELLED.equals(orderEntry.getStatus())) {
                throw new ManagerException(SimpleErrorCodes.ORDER_ALREADY_CANCELLED, new String[]{orderId.toString(),OrderStatus.DELIVERED.name()});
            } else if (OrderStatus.DELIVERED.equals(orderEntry.getStatus())) {
                throw new ManagerException(SimpleErrorCodes.ORDER_ALREADY_DELIVERED, new String[]{orderId.toString(),OrderStatus.SHIPPED.name()});
            } else if (OrderStatus.SHIPPED.equals(orderEntry.getStatus())) {
                throw new ManagerException(SimpleErrorCodes.ORDER_ALREADY_SHIPPED, new String[]{orderId.toString(),OrderStatus.SHIPPED.name()});
            }
            // reserve inventory
            LOGGER.info("Reserved inventory");
            // Add comment about confirmation
            LOGGER.info("Added comments");
            // Send confirmation email to customer

            PublishMessage.sendOrderConfirmNotification(orderEntry);
            LOGGER.info("Email to customer");

            orderEntry.setStatus(OrderStatus.CONFIRMED);

            ContextInfo info = Context.getContextInfo();
            String[] renderKeys = new String[] {};
            Object[] renderValues = new Object[] {};

            try {
                EmailServiceClient.sendEmailTemplateAmazon(null, "bhaskar.saraogi@gmail.com", "bhaskar.saraogi@myntra.com", null, "support@myntra.com", "Test email", MimeType
                        .TEXT_HTML.toString(), "simple_erp", renderKeys, renderValues, info);
            } catch (ERPServiceException e) {
                e.printStackTrace();
            }

            return update(orderEntry, orderEntry.getId());



        } else {
            LOGGER.info("Order already confirmed");
            return orderEntry;
        }
    }

    public void setOrderItemManager(OrderItemManager orderItemManager) {
        this.orderItemManager = orderItemManager;
    }
    
}
