package com.myntra.simple.manager;

import com.myntra.commons.manager.BaseManager;
import com.myntra.simple.client.entry.CancellationReasonEntry;
import com.myntra.simple.entities.CancellationReason;

/**
 * Created by vedulla.krishna - 11153 on 17/08/15.
 */
public interface CancellationReasonManager extends BaseManager<CancellationReasonEntry, CancellationReason> {

}
