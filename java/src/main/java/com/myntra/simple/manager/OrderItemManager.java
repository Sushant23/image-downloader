package com.myntra.simple.manager;

import com.myntra.commons.manager.BaseManager;
import com.myntra.simple.client.entry.OrderItemEntry;
import com.myntra.simple.entities.OrderItem;

/**
 * Created by pavankumar.at on 05/08/15.
 */
public interface OrderItemManager extends BaseManager<OrderItemEntry, OrderItem> {
}
