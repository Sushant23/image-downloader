package com.myntra.simple.manager.impl;

import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.simple.client.entry.OrderItemEntry;
import com.myntra.simple.entities.OrderItem;
import com.myntra.simple.manager.OrderItemManager;

/**
 * Created by pavankumar.at on 05/08/15.
 */
public class OrderItemManagerImpl extends BaseManagerImpl<OrderItemEntry, OrderItem> implements OrderItemManager{

}
