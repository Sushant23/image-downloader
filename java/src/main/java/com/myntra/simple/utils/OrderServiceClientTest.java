package com.myntra.simple.utils;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.utils.Context;
import com.myntra.simple.client.OrderClient;
import com.myntra.simple.client.codes.SimpleErrorCodes;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created by pavankumar.at on 03/08/15.
 */
public class OrderServiceClientTest {

    private static final String BASE_URL = "http://localhost:7999/";
    private static final String SERVICE_NAME = "myntra-simple-service/";
    private static final String SERVICE_URL = BASE_URL + SERVICE_NAME;
    private static final Logger logger = LoggerFactory.getLogger(OrderServiceClientTest.class);
    private static int count = 0;

    @Test
    public void testGetOrder() {
        try {
            OrderClient.getOrder("http://localhost:7999/myntra/", 12L, Context.getContextInfo());
        } catch (ERPServiceException e) {
            logger.error("Error fetching order: ", e);
            if (e.getCode() == SimpleErrorCodes.ORDER_NOT_FOUND_HERE.getStatusCode()) {
                System.out.println("Order is not found");
            }
        }
    }

    @Test
    public void testConfirmOrder() {
        try {
            OrderClient.confirmOrder("http://localhost:7999/myntra/", 12L, Context.getContextInfo());
        } catch (ERPServiceException e) {
            logger.error("Error fetching order: ", e);
            if (e.getCode() == SimpleErrorCodes.ORDER_NOT_FOUND_HERE.getStatusCode()) {
                System.out.println("Order is not found");
                // Some business case here
            } else if (e.getCode() == SimpleErrorCodes.ORDER_ALREADY_DECLINED.getStatusCode()) {
                System.out.println("Order is already declined. You do not have to retry");
            }
        }
    }
}
