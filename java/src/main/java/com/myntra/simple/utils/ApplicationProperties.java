package com.myntra.simple.utils;

/**
 * Created by pavankumar.at on 15/07/15.
 */
public interface ApplicationProperties {

    public String DISCOUNT_AMOUNT = "simple.discount_amount";
    public String RANDOM_ORDER = "simple.random_order";
    public String GREETING_NAME = "simple.greeting_value";

}
