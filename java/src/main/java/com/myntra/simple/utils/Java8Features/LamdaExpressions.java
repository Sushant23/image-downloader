package com.myntra.simple.utils.Java8Features;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by pavankumar.at on 04/08/15.
 */
public class LamdaExpressions {

    public static void main(String args[]) {
        List<String> brands = Arrays.asList("nike", "adidas", "lakhani", "local", "footpath");
        System.out.println(brands);
        // Welcome to Java 8
        Collections.sort(brands, (a, b) -> a.compareTo(b));

        System.out.println(brands);
    }

}
