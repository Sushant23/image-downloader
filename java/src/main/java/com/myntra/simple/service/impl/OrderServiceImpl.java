package com.myntra.simple.service.impl;

import com.myntra.commons.codes.ERPErrorCodes;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ManagerException;
import com.myntra.commons.exception.runtime.PseudoRuntimeException;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.simple.client.codes.SimpleErrorCodes;
import com.myntra.simple.client.codes.SimpleSuccessCodes;
import com.myntra.simple.client.entry.OrderEntry;
import com.myntra.simple.client.response.OrderResponse;
import com.myntra.simple.entities.Order;
import com.myntra.simple.manager.OrderManager;
import com.myntra.simple.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pavankumar.at on 15/07/15.
 */
public class OrderServiceImpl extends BaseServiceImpl<OrderResponse, OrderEntry, Order> implements OrderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderServiceImpl.class.getName());

    public OrderServiceImpl() {
        super(SimpleSuccessCodes.ORDER_ADDED_SUCCESSFULLY, SimpleSuccessCodes.ORDER_FETCHED_SUCCESSFULLY, SimpleSuccessCodes.ORDER_UPDATED_SUCCESSFULLY, SimpleErrorCodes.ORDER_NOT_FOUND_HERE);
    }

    @Override
    public OrderResponse getRandomOrders(Long count) {
        try {
            List<OrderEntry> orderEntryList = ((OrderManager) manager).getRandom(count);
            OrderResponse response = createResponse(orderEntryList);
            response.setStatus(new StatusResponse(SimpleSuccessCodes.ORDER_FETCHED_SUCCESSFULLY, StatusResponse.Type.SUCCESS));
            return response;
        } catch (Exception e) {
            LOGGER.error("Unable to retrieve random orders", e);
            return new OrderResponse(new StatusResponse(SimpleErrorCodes.ERR_NO_DATA_TO_PROCESS.getStatusCode(), e.getMessage(), StatusResponse.Type.ERROR));
        }
    }

    @Override
    public String getHelloWorld() {
        return "Hello World";
    }

    @Override
    public String getRegionCode(Long pincode) {
        try {
            return ((OrderManager) manager).getRegionCode(pincode);
        } catch (ManagerException e) {
            LOGGER.error("Unable to retrieve pincode");
            return e.getMessage();
        }
    }

    @Override
    public OrderResponse confirmOrder(Long orderId) {
        try {
            OrderEntry orderEntry = ((OrderManager) manager).confirmOrder(orderId);
            List<OrderEntry> orderEntries = new ArrayList<>();
            orderEntries.add(orderEntry);
            OrderResponse response = createResponse(orderEntries);
            response.setStatus(new StatusResponse(SimpleSuccessCodes.ORDER_UPDATED_SUCCESSFULLY, StatusResponse.Type.SUCCESS));
            return response;
        } catch (ManagerException e) {
            LOGGER.error("Unable to confirm order: ", e);
            return new OrderResponse(e.getStatusResponse());
        } catch(PseudoRuntimeException e){
            LOGGER.error("Failed to confirm order (Runtime): ", e);
            return new OrderResponse(e.getStatusResponse());
        } catch (Exception e) {
            LOGGER.error("Failed to update confirm order: ", e);
            return new OrderResponse(new StatusResponse(ERPErrorCodes.ERR_RETRIEVING, StatusResponse.Type.ERROR));
        }
    }

    @Override
    protected OrderResponse createResponse(List<OrderEntry> orderEntries) {
        return new OrderResponse(orderEntries);
    }


}
