package com.myntra.simple.service;

import com.myntra.commons.service.BaseService;
import com.myntra.simple.client.entry.OrderEntry;
import com.myntra.simple.client.response.OrderResponse;
import com.myntra.simple.entities.Order;

import javax.ws.rs.*;

/**
 * Created by pavankumar.at on 15/07/15.
 */
@Path("/order/")
public interface OrderService extends BaseService<OrderResponse, OrderEntry, Order> {

    @GET
    @Produces({"application/xml", "application/json"})
    @Path("/getRandomOrders/{count}")
    public OrderResponse getRandomOrders(@PathParam("count") Long count);

    @GET
    @Produces("text/plain")
    @Path("/helloWorld")
    public String getHelloWorld();

    @GET
    @Produces("text/plain")
    @Path("/getRegion/{pincode}")
    public String getRegionCode(@PathParam("pincode") Long pincode);

    //http://restcookbook.com/HTTP%20Methods/put-vs-post/

    @PUT
    @Produces({"application/xml", "application/json"})
    @Path("/confirm/{orderId}")
    public OrderResponse confirmOrder(@PathParam("orderId") Long orderId);

}
