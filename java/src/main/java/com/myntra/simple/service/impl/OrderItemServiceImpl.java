package com.myntra.simple.service.impl;

import com.myntra.commons.codes.StatusCodes;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.simple.client.codes.SimpleErrorCodes;
import com.myntra.simple.client.codes.SimpleSuccessCodes;
import com.myntra.simple.client.entry.OrderItemEntry;
import com.myntra.simple.client.response.OrderItemResponse;
import com.myntra.simple.entities.OrderItem;
import com.myntra.simple.service.OrderItemService;

import java.util.List;

/**
 * Created by pavankumar.at on 05/08/15.
 */
public class OrderItemServiceImpl extends BaseServiceImpl<OrderItemResponse, OrderItemEntry, OrderItem> implements OrderItemService {

    public OrderItemServiceImpl(StatusCodes statusAdded, StatusCodes statusRetrieved, StatusCodes statusUpdated, StatusCodes notFoundStatus) {
        super(statusAdded, statusRetrieved, statusUpdated, notFoundStatus);
    }

    public OrderItemServiceImpl() {
        super(SimpleSuccessCodes.ORDER_ADDED_SUCCESSFULLY, SimpleSuccessCodes.ORDER_FETCHED_SUCCESSFULLY, SimpleSuccessCodes.ORDER_UPDATED_SUCCESSFULLY, SimpleErrorCodes.ORDER_NOT_FOUND_HERE);
    }

    @Override
    protected OrderItemResponse createResponse(List<OrderItemEntry> orderItemEntries) {
        return new OrderItemResponse(orderItemEntries);
    }
}
