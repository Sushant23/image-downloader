package com.myntra.simple.service.impl;

import com.myntra.kafka.producer.KafkaProducerClient;
import com.myntra.simple.client.entry.ImageDownloaderEntry;
import com.myntra.simple.client.response.ImageDownloaderResponse;
import com.myntra.simple.handler.FetchS3ResourceHandler;
import com.myntra.simple.service.ImageDownloaderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by 11118 on 28/01/16.
 */
public class ImageDownloaderServiceImpl implements ImageDownloaderService {

    private final Logger LOGGER = LoggerFactory.getLogger(ImageDownloaderService.class);

    @Autowired
    KafkaProducerClient kafkaProducerClient;


    @Override
    public ImageDownloaderResponse fetchS3ResourceWithoutQueueing(ImageDownloaderEntry source) {

        LOGGER.info("Received the source: " + source.toString());

        FetchS3ResourceHandler handler = FetchS3ResourceHandler.getInstance();

        ImageDownloaderResponse response = handler.fetchS3Resource(source);

        LOGGER.info("Returning the response: " + response.toString());

        return response;
    }

    @Override
    public void pushS3Resource(String  source) {

        LOGGER.info("Received message to push to Kafka: " + source);
        kafkaProducerClient.pushToQueue(source);
        LOGGER.info("Successfully pushed message to Kafka");


    }

}
