package com.myntra.simple.service;

import com.myntra.commons.service.BaseService;
import com.myntra.simple.client.entry.OrderItemEntry;
import com.myntra.simple.client.response.OrderItemResponse;
import com.myntra.simple.entities.OrderItem;

import javax.ws.rs.Path;

/**
 * Created by pavankumar.at on 05/08/15.
 */
@Path("/orderitem/")
public interface OrderItemService  extends BaseService<OrderItemResponse, OrderItemEntry, OrderItem> {
}
