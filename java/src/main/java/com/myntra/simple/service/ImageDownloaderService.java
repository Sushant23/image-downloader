package com.myntra.simple.service;

import com.myntra.simple.client.entry.ImageDownloaderEntry;
import com.myntra.simple.client.response.ImageDownloaderResponse;

import javax.ws.rs.*;

/**
 * Created by 11118 on 28/01/16.
 */

@Path("/")
public interface ImageDownloaderService {

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("/fetch")
    public ImageDownloaderResponse fetchS3ResourceWithoutQueueing(ImageDownloaderEntry source);


    @PUT
    @Path("/push")
    public void pushS3Resource(String source);

}
