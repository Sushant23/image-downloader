package com.myntra.simple.handler;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.simple.client.codes.SimpleSuccessCodes;
import com.myntra.simple.client.entry.ImageDownloaderEntry;
import com.myntra.simple.client.response.ImageDownloaderResponse;
import com.myntra.simple.listeners.SpringContextListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.JedisCluster;

/**
 * Created by 11118 on 10/05/16.
 */
public class FetchS3ResourceHandler {

    private JedisCluster jedisCluster = null;
    private static FetchS3ResourceHandler instance = null;
    private static final Logger LOGGER = LoggerFactory.getLogger(FetchS3ResourceHandler.class);

    private FetchS3ResourceHandler() {

    }

    public static FetchS3ResourceHandler getInstance() {
        LOGGER.info("FETCHING INSTANCE OF FETCHS3RESOURCEHANDLER METHOD");
        if (instance == null)
            instance = new FetchS3ResourceHandler();
        return instance;
    }

    public ImageDownloaderResponse fetchS3Resource(ImageDownloaderEntry source) {
        // TODO: Ensure jedisCluster is not NULL
        jedisCluster = SpringContextListener.jedisCluster;

        LOGGER.info("Source Received is: " + source.toString());
        ImageDownloaderResponse response = new ImageDownloaderResponse();
        StatusResponse status = new StatusResponse();

        try {
            String sourceImageURL = source.getSourceImageURL();
            response.setSourceImageURL(sourceImageURL);
            LOGGER.info("Checking if key exists in Redis!");


            if (jedisCluster.exists(sourceImageURL)) {
                LOGGER.info("Found URL in Redis!");

                String S3URL = jedisCluster.get(sourceImageURL);
                status.setStatusType(StatusResponse.Type.SUCCESS);
                status.setStatusCode(SimpleSuccessCodes.S3_LINK_FETCHED_SUCCESSFULLY.getStatusCode());
                response.setS3ImageURL(S3URL);

            } else {
                LOGGER.info("Didn't find URL in Redis!");
                ImageDownloadHandler imageDownloader = new ImageDownloadHandler();


                LOGGER.info("Downloading Image.");
                imageDownloader.downloadImage(sourceImageURL);
                LOGGER.info("Successfully downloaded image");

                S3ImageUploadHandler imageUploader = S3ImageUploadHandler.getInstance();
                LOGGER.info("Starting Image Upload to S3");
                if (imageUploader.uploadFileToS3(imageDownloader.getImageData(), sourceImageURL, imageDownloader.getContentType())) {
                    LOGGER.info("Successfully uploaded file to S3");
                    LOGGER.info("Adding mapping in Redis");
                    String destinationURL = "s3://" + imageUploader.getBucket() + "/" + imageUploader.getFolderName() + sourceImageURL;
                    jedisCluster.set(sourceImageURL, destinationURL);
                    LOGGER.info("Successfully added mapping in Redis");
                    status.setStatusType(StatusResponse.Type.SUCCESS);
                    status.setStatusCode(SimpleSuccessCodes.S3_LINK_GENERATED_SUCCESSFULLY.getStatusCode());
                    response.setS3ImageURL(destinationURL);
                } else {
                    LOGGER.error("FAILED TO UPLOAD IMAGE TO S3");
                    response.setS3ImageURL("");
                    status.setStatusCode(SimpleSuccessCodes.S3_LINK_GENERATION_FAILED.getStatusCode());
                    status.setStatusType(StatusResponse.Type.ERROR);
                }
            }
        }
        catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage());
            LOGGER.error("Exception occurred: " + ex.getMessage());
            LOGGER.error("Exception: " + ex);
        }
        response.setResponse(status);
        return response;
    }

    public static void main(String[] args) {
        FetchS3ResourceHandler test = FetchS3ResourceHandler.getInstance();
        String imgURL = "http://ecx.images-amazon.com/images/I/714OlCDY47L._UL1L5003_.jpg";
        ImageDownloaderEntry entry = new ImageDownloaderEntry();
        entry.setSourceImageURL(imgURL);
        test.fetchS3Resource(entry);
    }

}
