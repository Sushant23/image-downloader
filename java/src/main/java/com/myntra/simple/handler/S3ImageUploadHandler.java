package com.myntra.simple.handler;

import org.apache.commons.codec.digest.DigestUtils;
import org.jets3t.service.S3Service;
import org.jets3t.service.S3ServiceException;
import org.jets3t.service.impl.rest.httpclient.RestS3Service;
import org.jets3t.service.model.S3Object;
import org.jets3t.service.security.AWSCredentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by 11118 on 10/05/16.
 */
public class S3ImageUploadHandler {

    private String accessKey, secretKey, bucket, folderName;
    private AWSCredentials awsCredentials;
    private S3Service s3Service;
    private Logger LOGGER = LoggerFactory.getLogger(S3ImageUploadHandler.class.getName());
    private static S3ImageUploadHandler instance = null;

    private S3ImageUploadHandler() {
        // TODO: Load properties from conf folder
        LOGGER.info("Loading S3 Configs");
        accessKey = "AKIAJ7UMKDLJ5NKR4THQ";
        secretKey = "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";
        bucket = "myntra";
        folderName = "scm-inbound/fashionStore-qa/fdb_images/";
        awsCredentials = new AWSCredentials(accessKey, secretKey);
        s3Service = new RestS3Service(awsCredentials);

    }

    public static S3ImageUploadHandler getInstance() {
        if (instance == null)
            instance = new S3ImageUploadHandler();
        return instance;
    }

    public boolean uploadFileToS3(byte[] bytes, String fileName, String contentType) {
        try {
            LOGGER.info("Uploading image to S3");
            fileName = DigestUtils.md5Hex(fileName) + "_" +  fileName.substring(fileName.lastIndexOf('/')+1);
            // TODO: Handle case where bytes is null
            S3Object s3Object = new S3Object(folderName + fileName, bytes);
            s3Object.setBucketName(bucket);
            s3Object.setContentType(contentType);
            s3Service.putObject(bucket, s3Object);
            LOGGER.info("Successfully uploaded image to S3");
            return true;
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Failed to upload image: ",e.getMessage());
            return false;
        } catch (IOException e) {
            LOGGER.error("Failed to upload image: ",e.getMessage());
            return false;
        } catch (S3ServiceException e) {
            LOGGER.error("Failed to upload image: ",e.getMessage());
            return false;
        }
    }

    public String getBucket() {
        return bucket;
    }

    public String getFolderName() {
        return folderName;
    }

}
