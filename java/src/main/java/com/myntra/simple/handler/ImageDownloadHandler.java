package com.myntra.simple.handler;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.TimeUnit;

/**
 * Created by 11118 on 10/05/16.
 */
public class ImageDownloadHandler {

    private Logger LOGGER = LoggerFactory.getLogger(ImageDownloadHandler.class.getName());
    private byte[] imageData;


    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public byte[] getImageData() {
        return imageData;
    }

    public void setImageData(byte[] imageData) {
        this.imageData = imageData;
    }

    private String contentType;

    private void applyDelay(int retry) {
        LOGGER.info("APPLYING DELAY");
        double random = Math.random()*200+400;
        double multiplier = Math.pow(2, retry);
        double delay = ((multiplier * random) / 50) + random;
        LOGGER.info("Sleeping for " + delay + " seconds");

        try {
            TimeUnit.MILLISECONDS.sleep((long) delay);
        } catch (InterruptedException e) {
            LOGGER.error("Connection thread killed.", e.getMessage() );
        }
    }

    public void downloadImage(String url) {
        LOGGER.info("Downloading Image:");
        int maxRetries = 10;
        URLConnection urlConnection;
        HttpURLConnection httpURLConnection = null;
        InputStream inputStream = null;
        for (int i = 0; i <= maxRetries; i++) {
            try {
                LOGGER.info("Hitting url : " + url);
                URL urlObject = new URL(url);
                urlConnection = urlObject.openConnection();
                httpURLConnection = (HttpURLConnection) urlConnection;
                contentType = httpURLConnection.getContentType();
                inputStream = httpURLConnection.getInputStream();
                imageData = IOUtils.toByteArray(inputStream);
                LOGGER.info("Downloaded Image!!!");
                break;
            } catch (Exception e) {
                LOGGER.info("Retry no. : " + i + " " + e.getMessage());
                applyDelay(i);
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        LOGGER.error(e.getMessage(),e);
                    }
                }

                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }

            }
        }
    }

}
