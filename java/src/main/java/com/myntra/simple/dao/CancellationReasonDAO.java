package com.myntra.simple.dao;

import com.myntra.commons.dao.BaseDAO;
import com.myntra.simple.entities.CancellationReason;

/**
 * Created by vedulla.krishna - 11153 on 17/08/15.
 */
public interface CancellationReasonDAO extends BaseDAO<CancellationReason> {

}
