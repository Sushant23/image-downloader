package com.myntra.simple.dao.impl;

import com.myntra.commons.dao.impl.BaseDAOImpl;
import com.myntra.simple.dao.OrderItemDAO;
import com.myntra.simple.entities.OrderItem;

/**
 * Created by pavankumar.at on 05/08/15.
 */
public class OrderItemDAOImpl extends BaseDAOImpl<OrderItem> implements OrderItemDAO {
}
