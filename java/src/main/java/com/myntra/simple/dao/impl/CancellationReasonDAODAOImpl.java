package com.myntra.simple.dao.impl;

import com.myntra.commons.dao.impl.BaseDAOImpl;
import com.myntra.simple.dao.CancellationReasonDAO;
import com.myntra.simple.entities.CancellationReason;

/**
 * Created by vedulla.krishna - 11153 on 17/08/15.
 */
public class CancellationReasonDAODAOImpl extends BaseDAOImpl<CancellationReason> implements CancellationReasonDAO {

}
