package com.myntra.simple.dao;

import com.myntra.commons.dao.BaseDAO;
import com.myntra.simple.entities.Order;

/**
 * Created by pavankumar.at on 15/07/15.
 */
public interface OrderDAO extends BaseDAO<Order>{
}
