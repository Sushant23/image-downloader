package com.myntra.simple.dao;

import com.myntra.commons.dao.BaseDAO;
import com.myntra.simple.entities.OrderItem;

/**
 * Created by pavankumar.at on 05/08/15.
 */
public interface OrderItemDAO extends BaseDAO<OrderItem>{
}
