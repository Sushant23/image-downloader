package com.myntra.simple.dao.impl;

import com.myntra.commons.dao.impl.BaseDAOImpl;
import com.myntra.simple.dao.OrderDAO;
import com.myntra.simple.entities.Order;

/**
 * Created by pavankumar.at on 15/07/15.
 */
public class OrderDAOImpl extends BaseDAOImpl<Order> implements OrderDAO {
}
