package com.myntra.simple.entities;

import com.myntra.commons.entities.BaseEntity;
import com.myntra.simple.enums.OrderItemStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

/**
 * Created by pavankumar.at on 03/08/15.
 */
@Entity
@Table(name="order_item")
public class OrderItem extends BaseEntity{

    @Column(name = "mrp")
    private Double mrp;

    @Column(name = "sku_id")
    private Long skuId;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private OrderItemStatus status;

    @Column(name = "trade_discount")
    private Double tradeDiscount;

    @Column(name = "coupon_discount")
    private Double couponDiscount;

    @Column(name = "quantity")
    private Long quantity;

    @Column(name = "order_id")
    private Long orderId;

    public Double getMrp() {
        return mrp;
    }

    public void setMrp(Double mrp) {
        this.mrp = mrp;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public OrderItemStatus getStatus() {
        return status;
    }

    public void setStatus(OrderItemStatus status) {
        this.status = status;
    }

    public Double getTradeDiscount() {
        return tradeDiscount;
    }

    public void setTradeDiscount(Double tradeDiscount) {
        this.tradeDiscount = tradeDiscount;
    }

    public Double getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(Double couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }
}
