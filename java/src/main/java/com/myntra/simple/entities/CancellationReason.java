package com.myntra.simple.entities;

import com.myntra.commons.entities.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by vedulla.krishna - 11153 on 17/08/15.
 */

@Entity
@Table(name = "cancellation_details")
public class CancellationReason extends BaseEntity {

    @Column(name = "code")
    private int code;

    @Column(name = "description")
    private String description;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Cancellation Details: {" +
                " code: " + code +
                ", description: " + description +
                "}";
    }
}
