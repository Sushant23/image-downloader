package com.myntra.simple.entities;

import com.myntra.commons.entities.BaseEntity;
import com.myntra.simple.enums.OrderStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by pavankumar.at on 15/07/15.
 */
@Entity
@Table(name="myntra_order")
public class Order extends BaseEntity{

    @Column(name = "login_id")
    private String loginId;

    @Column(name = "mrp")
    private Double mrp;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    @Column(name = "discount")
    private Double discount;

    @Column(name = "cancellation_id")
    private Long cancellationId;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "order_id", updatable = false)
    private List<OrderItem> orderItems;

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public Double getMrp() {
        return mrp;
    }

    public void setMrp(Double mrp) {
        this.mrp = mrp;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Long getCancellationId() { return cancellationId;}

    public void setCancellationId(Long cancellationId) { this.cancellationId = cancellationId; }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    @Override
    public String toString() {
        return "Order{" +
                "loginId='" + loginId + '\'' +
                ", mrp=" + mrp +
                ", status=" + status +
                ", discount=" + discount +
                ", cancellationId=" + cancellationId +
                ", orderItems=" + orderItems +
                '}';
    }
}
